import pygame
import random
pygame.init()

white=(255,255,255)
black=(0,0,0)
red=(255,0,0)
green=(0,155,0)
display_width=800
display_height=600
block_size=20
FPS=10
appleimg=pygame.image.load('apple.jpg')


AppleThickness=30

gameDisplay=pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('Slytherin')
pygame.display.set_icon(appleimg)

smallfont=pygame.font.SysFont("comicsansms",30)
medfont=pygame.font.SysFont("comicsansms",50)
largefont=pygame.font.SysFont("comicsansms",80)

clock=pygame.time.Clock()

def game_intro():
    intro=True
    while intro:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                pygame.quit()
                quit()
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_c:
                    gameLoop()
                    intro=False
                elif event.key==pygame.K_q:
                    pygame.quit()
                    quit()
                    intro=False
        gameDisplay.fill(white)
        message_to_screen("welcome to Slytherin",green,-100,"large")
        message_to_screen("press c to continue or q to exit or p to pause", black, -30, "medium")
        clock.tick(5)
        pygame.display.update()


def snake(block_size,snakeList):
    for xny in snakeList:
       pygame.draw.rect(gameDisplay,black,[xny[0],xny[1],block_size,block_size])

def randAppGen():
    randAppleX = round((random.randrange(0, display_width-AppleThickness)) / 10.0) * 10.0
    randAppleY = round((random.randrange(0, display_height-AppleThickness)) / 10.0) * 10.0

    return randAppleX,randAppleY

def Score(score):
    result=smallfont.render("Score: " +str(score),True,black)
    gameDisplay.blit(result,[0,0])

def pause():
    paused=True
    while paused:

             for event in pygame.event.get():

                    if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()


                    if event.type == pygame.KEYDOWN:

                            if event.key == pygame.K_q:
                                    pygame.quit()
                                    quit()
                            if event.key == pygame.K_c:
                                    paused=False

                    message_to_screen("paused",green,-100,"large")
                    message_to_screen("press c to continue or q to exit",black,-30,"medium")

                    pygame.display.update()


def text_objects(text,color,size):
    if size=="small":
        textSurface=smallfont.render(text,True,color)
    elif size=="medium":
        textSurface = medfont.render(text, True, color)
    elif size=="large":
        textSurface = largefont.render(text, True, color)
    return textSurface,textSurface.get_rect()

def message_to_screen(mess,color,y_displacement,size):
    textSurf, textRect = text_objects(mess,color,size)
    textRect.center=(display_width/2),(display_height/2)+y_displacement
    gameDisplay.blit(textSurf,textRect)
    #screen_text=font.render(mess,True,color)
    #gameDisplay.blit(screen_text,[display_width/2,display_height/2])

def gameLoop():

        gameExit = False
        gameOver=False
        lead_x = display_width / 2
        lead_y = display_height / 2
        snakelist=[]
        snakeLength=1

        lead_x_change = 0
        lead_y_change = 0
        randAppleX,randAppleY=randAppGen()

        while not gameExit:
            while gameOver==True:
                

                message_to_screen("Game Over", red,-50,size="large")
                message_to_screen("for continue press c or to exit press q",black,10,size="small")
                pygame.display.update()
                for event in pygame.event.get():

                   if event.type==pygame.QUIT:
                       gameOver=False
                       gameExit=True

                   if event.type==pygame.KEYDOWN:

                     if event.key==pygame.K_q:
                        gameOver=False
                        gameExit=True
                     if event.key==pygame.K_c:
                        gameLoop()

            for event in pygame.event.get():
                if event.type==pygame.QUIT:
                    gameExit=True
                if event.type==pygame.KEYDOWN:
                    if event.key==pygame.K_LEFT:
                        lead_x_change=-block_size
                        lead_y_change=0

                    elif event.key==pygame.K_RIGHT:
                        lead_x_change=block_size
                        lead_y_change = 0
                    elif event.key == pygame.K_UP:
                        lead_y_change = -block_size
                        lead_x_change = 0
                    elif event.key == pygame.K_DOWN:
                        lead_y_change = block_size
                        lead_x_change = 0
                    elif event.key==pygame.K_p:
                        pause()

            if lead_x >= display_width-block_size or lead_x < 0 or lead_y >= display_height-block_size or lead_y < 0:
                gameOver=True

            snakeHead=[]
            snakeHead.append(lead_x)
            snakeHead.append(lead_y)
            snakelist.append(snakeHead)
            if len(snakelist)>snakeLength:
                del snakelist[0]

            for eachSegament in snakelist[:-1]:
                if eachSegament==snakeHead:
                    gameOver=True
            lead_x+=lead_x_change
            lead_y+=lead_y_change

            gameDisplay.fill(white)
            snake(block_size,snakelist)
            pygame.draw.rect(gameDisplay,red,[randAppleX,randAppleY,AppleThickness,AppleThickness])
            score=snakeLength-1
            Score(score)
            pygame.display.update()
            if (lead_x > randAppleX and lead_x < randAppleX+AppleThickness) or (lead_x + block_size > randAppleX and lead_x+block_size < randAppleX + AppleThickness):
                if lead_y>=randAppleY and lead_y<=randAppleY +AppleThickness:
                    snakeLength += 1
                    randAppleX, randAppleY = randAppGen()

                elif lead_y + block_size>=randAppleY and lead_y+block_size <=randAppleY+AppleThickness:
                    snakeLength += 1
                    randAppleX, randAppleY = randAppGen()

            clock.tick(FPS)
        pygame.quit()
        quit()
game_intro()
gameLoop()