import pygame
import random
import numpy as np

pygame.init()

white = (255, 255, 255)
black = (0, 0, 0)
light_red=(255,0,0)
red = (200, 0, 0)
green = (0, 155, 0)
light_green=(0,255,0)
yellow=(200,200,0)
light_yellow=(255,255,0)
display_width = 800
display_height = 600

FPS = 10

tankWidth=40
tankHeight=20
turretwidth=5
wheelwidth=5
ground_height=35
fire_sound=pygame.mixer.Sound("ANNIGUN1.WAV")
explosion_sound=pygame.mixer.Sound("Torpedo+Explosion.wav")

gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('BattleField')


smallfont = pygame.font.SysFont("comicsansms", 30)
medfont = pygame.font.SysFont("comicsansms", 50)
largefont = pygame.font.SysFont("comicsansms", 80)

clock = pygame.time.Clock()


def Score(score):
    result = smallfont.render("Score: " + str(score), True, black)
    gameDisplay.blit(result, [0, 0])


def pause():
    paused = True
    while paused:

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_c:
                    paused = False

            message_to_screen("paused", green, -100, "large")
            message_to_screen("press c to continue or q to exit", black, -30, "medium")

            pygame.display.update()



def text_objects(text, color, size):
    if size == "small":
        textSurface = smallfont.render(text, True, color)
    elif size == "medium":
        textSurface = medfont.render(text, True, color)
    elif size == "large":
        textSurface = largefont.render(text, True, color)
    return textSurface, textSurface.get_rect()



def message_to_screen(mess, color, y_displacement, size):
    textSurf, textRect = text_objects(mess, color, size)
    textRect.center = (display_width / 2), (display_height / 2) + y_displacement
    gameDisplay.blit(textSurf, textRect)



def button(text,x,y,width,height,activecolor,inactivecolor,action=None):
    cur=pygame.mouse.get_pos()
    click=pygame.mouse.get_pressed()
    if x<cur[0]<x+width and y<cur[1]< y+height:
        pygame.draw.rect(gameDisplay,inactivecolor,(x,y,width,height))
        if click[0]==1 and action!=None:
            if action=="play":

                gameLoop()
            elif action=="controls":
                game_controls()
            elif action=="quit":
                pygame.quit()
                quit()
            elif action=="main":
                game_intro()

    else :
        pygame.draw.rect(gameDisplay,activecolor,(x,y,width,height))
    text_to_button(text,black,x,y,width,height,"small")



def text_to_button(text,color,x,y,width,height,size="small"):
     textSurf,textRect=text_objects(text,color,size)
     textRect.center=(x+width/2,y+height/2)
     gameDisplay.blit(textSurf,textRect)



def barrier(xlocation,randomHeight,barrier_width):

    pygame.draw.rect(gameDisplay,black,[xlocation,display_height-randomHeight,barrier_width,randomHeight])


def tank(x,y,turpos):
    x=int(x)
    y=int(y)
    possible_turrets=[(x-27,y-4),
                      (x-24,y-6),
                      (x-21,y-8),
                      (x-18,y-10),
                      (x-15,y-12),
                      (x-12,y-14),
                      (x-9,y-16),
                      (x-6,y-18)]
    pygame.draw.circle(gameDisplay,black,(x,y),int(tankHeight/2))
    pygame.draw.rect(gameDisplay,black,(x-(tankWidth/2),y,tankWidth,tankHeight))
    pygame.draw.line(gameDisplay,black,(x,y),possible_turrets[turpos],turretwidth)
    for i in range(21):
         pygame.draw.circle(gameDisplay,black,(x-int(tankWidth/2)+2*i,y+tankHeight),int(wheelwidth))
    return possible_turrets[turpos]


def enemy_tank(x,y,turpos):
    x=int(x)
    y=int(y)
    possible_turrets=[(x+27,y-4),
                      (x+24,y-6),
                      (x+21,y-8),
                      (x+18,y-10),
                      (x+15,y-12),
                      (x+12,y-14),
                      (x+9,y-16),
                      (x+6,y-18)]
    pygame.draw.circle(gameDisplay,black,(x,y),int(tankHeight/2))
    pygame.draw.rect(gameDisplay,black,(x-(tankWidth/2),y,tankWidth,tankHeight))
    pygame.draw.line(gameDisplay,black,(x,y),possible_turrets[turpos],turretwidth)
    for i in range(21):
         pygame.draw.circle(gameDisplay,black,(x-int(tankWidth/2)+2*i,y+tankHeight),int(wheelwidth))
    return possible_turrets[turpos]



def power(level):
    text=smallfont.render("Power:"+str(level)+"%",True,black)
    gameDisplay.blit(text,[display_width/2,0])


def fireShell(xy,tankx,tanky,turpos,gun_power,xlocation,barrir_width,randomHeigt,enemyTankx,enemyTanky):
    pygame.mixer.Sound.play(fire_sound)
    fire=True
    damage=0
    startingShell=list(xy)

    while fire:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        pygame.draw.circle(gameDisplay, red, (startingShell[0], startingShell[1]), 5)
        startingShell[0]-=(9-turpos)**2
        startingShell[1]+=int(((( startingShell[0]-xy[0])*0.015/(gun_power/50))**2-(turpos+turpos/(12-turpos))))
        if startingShell[1]>display_height-ground_height:

            hit_x = int((startingShell[0] * display_height) / startingShell[1])
            hit_y = int(display_height)-ground_height
            if enemyTankx - 10 <= hit_x <= enemyTankx + 10:
                damage = 25
            elif enemyTankx - 15 <= hit_x <= enemyTankx+ 15:
                damage = 20
            elif enemyTankx - 20 <= hit_x <= enemyTankx + 20:
                damage = 10

            elif enemyTankx - 25 <= hit_x <=enemyTankx + 25:
                damage = 7

            explosion(hit_x, hit_y,size=50)
            fire = False

        check_x_1=startingShell[0]<=xlocation+barrir_width
        check_x_2 =startingShell[0]>=xlocation
        check_y_1 =startingShell[1]>=display_height-randomHeigt
        check_y_2 =startingShell[1]<=display_height


        if check_x_1 and check_x_2 and check_y_1 and check_y_2:
               hit_x=int(startingShell[0])
               hit_y=int(startingShell[1])
               explosion(hit_x,hit_y,size=50)
               fire=False



        pygame.display.update()
        clock.tick(60)
    return damage



def e_fireShell(xy,enemyTankx,enemyTanky,turpos,gun_power,xlocation,barrir_width,randomHeigt,ptanx,ptanky):
    pygame.mixer.Sound.play(fire_sound)
    damage=0
    currentPower=1
    power_found=False
    while not power_found:
        currentPower+=1
        if currentPower>100:

            power_found=True
        fire = True
        startingShell = list(xy)
        while fire:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()


            startingShell[0] += (9 - turpos) ** 2
            startingShell[1] += int(
                (((startingShell[0] - xy[0]) * 0.015 / (currentPower/ 50)) ** 2 - (turpos + turpos / (12 - turpos))))
            if startingShell[1] > display_height - ground_height:
                hit_x = int((startingShell[0] * display_height) / startingShell[1])
                hit_y = int(display_height) - ground_height
                if ptanx-15<hit_x<ptanx+15:
                    power_found=True
                fire = False


    fire=True
    startingShell=list(xy)
    while fire:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        pygame.draw.circle(gameDisplay, red, (startingShell[0], startingShell[1]), 5)
        startingShell[0]+=(9-turpos)**2
        gun_power=random.randrange(int(.9*currentPower),int(1.10*currentPower))
        startingShell[1]+=int(((( startingShell[0]-xy[0])*0.015/(gun_power/50))**2-(turpos+turpos/(12-turpos))))
        if startingShell[1]>display_height-ground_height:

            hit_x = int((startingShell[0] * display_height) / startingShell[1])
            hit_y = int(display_height)-ground_height
            if ptanx-10<=hit_x<=ptanx+10:
                damage=25
            elif ptanx-15<=hit_x<=ptanx+15:
                damage = 20
            elif ptanx - 20 <= hit_x <= ptanx + 20:
                damage = 10

            elif ptanx - 25 <= hit_x <= ptanx + 25:
                damage = 7

            explosion(hit_x, hit_y, size=50)
            fire = False
        check_x_1 = startingShell[0] <= xlocation + barrir_width
        check_x_2 = startingShell[0] >= xlocation
        check_y_1 = startingShell[1] >= display_height - randomHeigt
        check_y_2 = startingShell[1] <= display_height

        if check_x_1 and check_x_2 and check_y_1 and check_y_2:
            hit_x = int(startingShell[0])
            hit_y = int(startingShell[1])
            explosion(hit_x, hit_y, size=50)
            fire = False





        pygame.display.update()
        clock.tick(60)
    return damage


def explosion(hit_x,hit_y,size=50):
    pygame.mixer.Sound.play(explosion_sound)
    explode=True

    colorchoices=[red,yellow,green,black,light_yellow,light_green]
    while explode:
       for event in pygame.event.get():
           if event.type==pygame.QUIT:
               pygame.quit()
               quit()

       magnitude=1
       while magnitude<=size:
                explode_x=hit_x+random.randrange(-1*magnitude,magnitude)
                explode_y=hit_y+random.randrange(-1*magnitude,magnitude)
                pygame.draw.circle(gameDisplay,colorchoices[random.randrange(0,4)],(explode_x,explode_y),random.randrange(1,4))
                magnitude+=1
                pygame.display.update()
                clock.tick(100)
       explode=False

def health_bars(player_health,enemy_health):
    if player_health > 75:
        player_health_color = green
    elif player_health > 50:
        player_health_color = yellow
    else:
        player_health_color = red

    if enemy_health > 75:
        enemy_health_color = green
    elif enemy_health > 50:
        enemy_health_color = yellow
    else:
        enemy_health_color = red

    pygame.draw.rect(gameDisplay, player_health_color, (680, 25, player_health, 25))

    pygame.draw.rect(gameDisplay, enemy_health_color, (20, 25, enemy_health, 25))


def game_over():
    game_over=True
    while game_over:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                pygame.quit()
                quit()

        gameDisplay.fill(white)

        message_to_screen("You Lose",red,-100,"large")
        button("play", 150, 500, 100, 50, red, light_red, "play")
        button("main", 350, 500, 100, 50, green, light_green, "main")
        button("quit", 550, 500, 100, 50, yellow, light_yellow, "quit")
        game_over=False
    pygame.display.update()
    clock.tick(30)


def u_won():
    won=True
    while won:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                pygame.quit()
                quit()

        gameDisplay.fill(white)

        message_to_screen("Heyy!!You Won",red,-100,"large")
        button("play", 150, 500, 100, 50, red, light_red, "play")
        button("main", 350, 500, 100, 50, green, light_green, "main")
        button("quit", 550, 500, 100, 50, yellow, light_yellow, "quit")
        won=False
    pygame.display.update()
    clock.tick(30)

def game_controls():
        gcount = True
        while gcount:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()

            gameDisplay.fill(white)
            message_to_screen("Controls", green, -100, "large")
            message_to_screen("Fire: spacebar", black, -30, "small")
            message_to_screen("Move Turret: Up and Down arrows",black, 0, "small")
            message_to_screen("Move Tank: Left and Right arrows", black,40, "small")
            button("play", 150, 500, 100, 50, red, light_red, "play")
            button("main", 350, 500, 100, 50, green, light_green, "main")
            button("quit", 550, 500, 100, 50, yellow, light_yellow, "quit")
            clock.tick(15)
            pygame.display.update()


def game_intro():
    intro = True
    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:
                    gameLoop()
                    intro = False
                elif event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                    intro = False
        gameDisplay.fill(white)
        message_to_screen("welcome to BattleField", green, -100, "large")
        message_to_screen("press c to continue or q to exit or p to pause", black, -30, "small")
        button("play",150,500,100,50,red,light_red,"play")
        button("controls",350,500,100,50,green,light_green,"controls")
        button("quit",550,500,100,50,yellow,light_yellow,"quit")
        clock.tick(15)
        pygame.display.update()


def gameLoop():
    gameExit = False
    gameOver = False
    mainTankx = display_width * .9
    mainTanky = display_height * .9
    tankmove=0
    currentturpos=0
    changetur=0
    barrier_width=50
    power_change=0
    fire_power=50

    player_health = 100
    enemy_health = 100
    enemy_damage=0

    xlocation = (display_width / 2) + random.randint(-0.1 * display_width, 0.1* display_width)
    randomHeight = random.randrange(display_height * 0.1, display_height * 0.6)

    #Enemy
    enemyTankx=display_width*0.1
    enemyTanky=display_height*0.9


    while not gameExit:



        if gameOver == True:
            # gameDisplay.fill(white)
            message_to_screen("Game Over", red, -50, size="large")
            message_to_screen("Press C to play again or Q to exit", black, 50)
            pygame.display.update()
            while gameOver == True:

                    message_to_screen("Game Over", red, -50, size="large")
                    message_to_screen("for continue press c or to exit press q", black, 10, size="small")
                    pygame.display.update()
                    for event in pygame.event.get():

                        if event.type == pygame.QUIT:
                            gameOver = False
                            gameExit = True

                        if event.type == pygame.KEYDOWN:

                            if event.key == pygame.K_q:
                                gameOver = False
                                gameExit = True
                            if event.key == pygame.K_c:
                                gameLoop()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                   tankmove=-10



                elif event.key == pygame.K_RIGHT:
                   tankmove=10

                elif event.key == pygame.K_UP:
                    changetur = 1
                elif event.key == pygame.K_DOWN:
                    changetur = -1
                elif event.key==pygame.K_SPACE:
                    damage=fireShell(gun,mainTankx,mainTanky,currentturpos,fire_power,xlocation,barrier_width,randomHeight,enemyTankx,enemyTanky)
                    enemy_health-=damage
                    possible=['f','r']
                    index=random.randrange(0,2)
                    for x in range(random.randrange(1,10)):

                        if display_width * 0.3 > enemyTankx> display_width * 0.03:
                                if possible[index]=="f":
                                    enemyTankx+=5
                                elif possible[index]=="r":
                                    enemyTankx-=5
                    enemy_damage=e_fireShell(enemy_gun, enemyTankx, enemyTanky, 7, 50, xlocation, barrier_width,
                              randomHeight,mainTankx,mainTanky)
                    player_health-=enemy_damage
                elif event.key==pygame.K_a:
                    power_change=-1
                elif event.key == pygame.K_d:
                    power_change= 1



                elif event.key == pygame.K_p:
                    pause()
            elif event.type==pygame.KEYUP:
                if event.key==pygame.K_DOWN or event.key==pygame.K_UP:
                    changetur=0
                if event.key==pygame.K_LEFT or  event.key==pygame.K_RIGHT:
                    tankmove=0
                if event.key==pygame.K_a or event.key==pygame.K_d:
                    power_change=0

        gameDisplay.fill(white)
        gun = tank(mainTankx, mainTanky, currentturpos)
        enemy_gun=enemy_tank(enemyTankx,enemyTanky,7)

        currentturpos+=changetur
        if currentturpos>=8:
            currentturpos=7
        if currentturpos<0:
            currentturpos=0
        mainTankx += tankmove
        fire_power+=power_change

        if mainTankx-(tankWidth/2)<xlocation+barrier_width+2:
            mainTankx+=5

        if fire_power<1:
            fire_power=1
        elif fire_power>100:
            fire_power=100
        power(fire_power)
        health_bars(player_health,enemy_health)
        barrier(xlocation,randomHeight,barrier_width)
        pygame.draw.rect(gameDisplay,green,(0,display_height-ground_height,display_width,ground_height))
        if player_health < 1:
            game_over()
        elif enemy_health < 1:
            u_won()
        pygame.display.update()


        clock.tick(FPS)
    pygame.quit()
    quit()


game_intro()
gameLoop()